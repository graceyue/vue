import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'
// import Test from '@/components/Test'
import Login from '@/pages/login'
import Profile from '@/pages/profile'
import BasicLayout from '@/pages/layout'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: 'dashboard',
      component: BasicLayout,
      children: [
        {
          path: 'dashboard',
          component: () => import('@/pages/dashboard/index'),
          name: 'Dashboard'
          // meta: { title: 'dashboard', icon: 'dashboard', noCache: true }
        }
      ]
    },
    // {
    //   path: '/dashboard',
    //   name: 'dashboard',
    //   component: Test
    // },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/currentUser',
      name: 'currentUser',
      component: Profile
    }
  ]
})
