
const prefix = 'outbook-workplace'
export function get (key) {
  const value = sessionStorage.getItem(`${prefix}-${key}`)
  try {
    return JSON.parse(value)
  } catch (error) {
    return value
  }
}

export function set (key, value) {
  const data = !value || typeof value === 'string' ? value : JSON.stringify(value)
  sessionStorage.setItem(`${prefix}-${key}`, data)
}

export function clean (key) {
  sessionStorage.removeItem(`${prefix}-${key}`)
}

export function setLoginMsg (data) {
  return sessionStorage.setItem('outbook-workplace-remember', data)
}

export function getLoginMsg () {
  return sessionStorage.getItem('outbook-workplace-remember')
}
