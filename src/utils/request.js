import axios from './axios'

const defaultHeaders = (hasToken = true) => {
  const headers = {
    'Content-Type': 'application/json',
    'X-Platform': 'web',
    'x-client-type': 'teacher',
    Accept: 'application/json'
  }
  return headers
}

export const Rest = {
  post: (url, data, headers) => {
    const options = {
      body: JSON.stringify(data || ''),
      headers: Object.assign(defaultHeaders(), headers)
    }
    return axios.post(url, options)
  },
  put: (url, data, headers) => {
    const options = {
      body: JSON.stringify(data || ''),
      headers: Object.assign(defaultHeaders(), headers)
    }
    return axios.put(url, options)
  },
  delete: (url, data, headers) => {
    const options = {
      body: JSON.stringify(data || ''),
      headers: Object.assign(defaultHeaders(), headers)
    }
    return axios.delete(url, options)
  },
  patch: (url, data, headers) => {
    const options = {
      body: JSON.stringify(data || ''),
      headers: Object.assign(defaultHeaders(), headers)
    }
    return axios.patch(url, options)
  },
  head: (url, headers) => {
    const options = {
      headers: Object.assign(defaultHeaders(), headers)
    }
    return axios.head(url, options)
  },
  get: (url, headers) => {
    const options = {
      headers: Object.assign(defaultHeaders(), headers)
    }
    return axios.get(url, options)
  }
}

export default (options) => {
  return axios(options)
}
