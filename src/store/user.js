// import axios from 'axios'
import moment from 'moment'
// import qs from 'qs'
import axios, { Rest } from '../utils/request'
import { set as setToken, get as getToken } from '../utils/storage'

const defaultHeaders = {
  credentials: 'include',
  headers: { 'Content-type': 'application/x-www-form-urlencoded' }
}

const user = {
  namespace: 'user',
  state: {
    currentUser: {},
    token: getToken('token'),
    setting: []
  },
  mutations: {
    USER_LOGIN (state, res) {
      if (res && res.access_token) {
        res.expired_at = moment().add(res.expires_in, 's')
        setToken('token', res)
      } else {
        setToken('token', null)
      }
    },
    USER_CURRENT (state, res) {
      state.currentUser = res.data
      setToken('user', res.data)
    },
    CURRENT_SETTING (state, res) {
      state.setting = res.data
    }
  },
  actions: {
    userLogin ({ commit }, user) {
      const data = {
        ...user,
        client_type: 'teacher',
        grant_type: 'password'
      }
      return axios({
        url: '/oauth/token',
        method: 'post',
        headers: defaultHeaders,
        data
      }).then((response) => {
        commit('USER_LOGIN', response.data)
      })
    },
    async currentUser ({ commit }, user) {
      const res = await Rest.get('/api/currentUser').then((res) => {
        commit('USER_CURRENT', res)
      })
      return res
    },
    async getSetting ({ commit }, user) {
      const res = await Rest.get('/api/settings').then((res) => {
        commit('CURRENT_SETTING', res.data)
        return res
      })
      console.log('res111', res)
      return res
    }
  }
}

export default user
