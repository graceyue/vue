import Vue from 'vue'
import Vuex from 'vuex'
import user from './user'
// import actions from './action'
import getters from './getters'

Vue.use(Vuex)

// const state = {}

// export default new Vuex.Store({
//   state,
//   actions,
//   mutations
// })
const store = new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production', // 在非生产环境下，使用严格模式
  modules: {
    user
  },
  getters
})

export default store
