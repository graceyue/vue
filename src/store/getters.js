const getters = {
  currentUser: state => state.user.currentUser,
  token: state => state.user.token,
  setting: state => state.user.setting
}
export default getters
