// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

import App from './App'
import VueRouter from 'vue-router'
import router from './router'
import store from './store/'

import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '@/styles/index.scss' // global css

Vue.config.productionTip = false

Vue.use(Element)
Vue.use(VueRouter)
const routers = new VueRouter({
  router
})

new Vue({
  routers,
  store,
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
}).$mount('#app')

/* eslint-disable no-new */
// new Vue({
//   el: '#app',
//   router,
//   components: { App },
//   template: '<App/>'
// })
